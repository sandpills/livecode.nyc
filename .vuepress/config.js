const CopyWebpackPlugin = require("copy-webpack-plugin");
const Settings = require("../global_settings.js");
const feedPlugin = require("vuepress-plugin-feed");

const feedOptions = {
  canonical_base: "https://livecode.nyc",
  posts_directories: [
    "/event/",
  ],
  sort: entries => entries.sort(e => new Date(e.date)).reverse(),
  count: 100,
};

module.exports = {
  base: '/',
  title: Settings.title,
  description: Settings.description,

  dest: "./public",

  themeConfig: {
    repo: Settings.repository,
    mailingList: Settings.mailingList,
    discord: Settings.discord,
    social: Settings.social,
    navLinks: Settings.navLinks,
    footerLinks: Settings.footerLinks
  },

  markdown: {
    anchor: {
      permalink: true,
      permalinkBefore: false,
      permalinkSymbol: ""
    }
  },

  plugins: [
    [ "feed", feedOptions ],
  ],

  configureWebpack: (config, isServer) => {
    return {
      plugins: [
        new CopyWebpackPlugin([
          { from: "image", to: "image" },
          { from: "static", to: "." }
        ]),
      ]
    };
  }
};
