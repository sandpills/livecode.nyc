---
name: Reckoner+=Matthew
image: https://images.squarespace-cdn.com/content/v1/57939139579fb36d1f7463a8/1571762234183-HI0QON34ZFV1WMBRI3ZP/ke17ZwdGBToddI8pDm48kK60W-ob1oA2Fm-j4E_9NQB7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0kD6Ec8Uq9YczfrzwR7e2Mh5VMMOxnTbph8FXiclivDQnof69TlCeE0rAhj6HUpXkw/IMG_20191018_215936.jpg
---
Reckoner+=Matthew is a live performance project exploring interplay between analog and digital improvisation using synthesizers and live coding to create beat music that rests on top of waveforms, probabilities and loop conditions. Reckoner is Sumanth Srinivasan. Matthew is Matthew Kaney.  


