---
Title: Contact
---

# Contact

The best way to stay in touch is by joining our [Discord](https://discord.gg/fcrAKbg). It is the primary communication channel we use to organize and announce shows, meetups, and more.

For social media, we are on [Twitter](https://twitter.com/livecodenyc), [Instagram](https://instagram.com/livecodenyc), and [tiktok](https://www.tiktok.com/@livecodenyc).

# Global Community

- [TOPLAP Forum](https://forum.toplap.org)
- [llllllll](https://llllllll.co/)