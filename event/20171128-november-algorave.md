---
title: November Algorave
location: Sunnyvale
date: 2017-11-28 8:00 PM
image: /image/2018-november-algorave.png
link: https://www.facebook.com/events/282713278903363
---

A live coded evening of the succession of repetitive conditionals

==Featuring ==

Scorpio Mouse

2050

Codie (Sicchio x Sarah GHP)

Sean Lee

Ulysses Popple

==Tickets==

$10

==Algorave==

[https://vimeo.com/228411462](https://vimeo.com/228411462)

==More Live Coding==

[https://algorave.com/about/](https://algorave.com/about/)
