---
title: Algorave NYC
location: Eyebeam
date: 2018-04-11 6:00 PM
image: /image/2018-april-eyebeam.jpg
link: https://www.facebook.com/events/1450671205060961
---

[ Eyebeam's Welcome Wednesday ]
Algorave, Hosted by Ramsey Nasser and LiveCode.NYC
April 11, 2018 / 6 – 11pm
Join us for an evening of live coded sounds and visuals made for dancing. Live coders program and change their computational systems as they are running, to produce real-time creative coded works.
Featuring:
2050
Ulysses Popple
Sean Lee
Michael Lee
Codie
Charlie Roberts
Kindohm
Ramsey Nasser
Free and open to the public, make sure to RSVP!
___
This event is part of Welcome Wednesday, our ongoing event series that offers a flexible, social time and space for current and former Eyebeam artists to share new ideas, works in progress, host conversations, and experiment with different formats.
