---
title: CRTical Mass
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2023-04-15 8:00 PM
duration: 4:30
image: /image/2023-CRTICAL_mass.png
categories: [algorave, music, art, computers]
---

# CRTICAL Mass

Join us as we bask in warm beams of electrons. All vis will be broadcast on a pirate TV station onto our collection of CRT televisions. 

LINEUP:

- PRESWERVE
- Mr SynAckSter + vaporwave.obj
- mgs + emptyflash
- sandpills + Char Stiles
- Maxwell Neely-Cohen + New Visions NYC
- MK + Voyde
- PLAY
- Dog Collar + gwenpri.me + Janie Jaffe
- Dan Gorelick + Tachyons+
