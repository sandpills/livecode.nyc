---
title: human.machine.interface
location: Persona NYC 202B Plymouth Street, Brooklyn NY
date: 2023-05-27 7:00 PM
duration: 4:00
image: /image/2023-human-machine-interface.png
categories: [algorave, music, art, computers]
link: https://alpha.persona.nyc/purchase/humanmachineinterface-6362d222
---

# human.machine.interface
livecode.nyc hosts << human.machine.interface >> at Persona NYC, an evening of livecode experimental audiovisual acts playing with the intertwining of humans and machines, glitch -isms, cyborg manifestos, and speculative futures.

May 27, 2023 // 6pm doors // 7pm-11pm show

With performances by:
- Matthew D Gantt
- Sylvia Ke + Viola He
- easterner + the poet engineer + cyber-choir Annie Beliveau, Joy Tamayo, Sophie Delphis, Sishel Claverie
- R Tyler + Voyde
- alsoknownasrox

...and installations by:
- Katarina Hoeger
- Snow
- VampireExec

PLEASE NOTE: THIS IS A SHOES OFF SPACE
