---
title: Caturday
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2023-08-19 8:00 PM
duration: 4:30
image: /image/caturday.png
categories: [algorave, music, art, computers]
link: https://withfriends.co/event/16501648/caturday

---

# Caturday

An algorave for Cats by cats.
Mask Required, 21+

<pre>
|Viz          | Audio
+---------------------------->
| The Glad Scientist     
| Cougars Are Cats Too    
| Shellylynnx | MrSynAckster
| Voyde       | Aerosoul
| KastaKila   | Ezra
| VampireExec | Archaic Reckoner
V
</pre>
