---
name: Shelly Xiong
image: https://thepapermachine.com/img/livecode.png
links:
    linkedin: https://www.linkedin.com/in/shellylynnx/
    tiktok: https://www.tiktok.com/@shellylynnx
    instagram: https://www.instagram.com/shellylynnx/
---

Creative technologist - retro tech enthusiast - sometimes educator - sharing my special interests