---
name: Viola He
image: https://raw.githubusercontent.com/sandpills/violastream/main/viola-img/IMG_0007%202.jpg
links:
    website: http://violand.xyz/
    instagram: https://www.instagram.com/v10101a/
    soundcloud: https://soundcloud.com/v10101a/

---

A manic pixel making computer beeps and electronics boops.